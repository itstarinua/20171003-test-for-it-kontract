<?php
require_once 'vendor/autoload.php';
use DStaroselskiy\Php\Test\TemperatureConverter\Controllers\IndexController;
use DStaroselskiy\Php\Test\TemperatureConverter\Components\TemplateController;

$controller = new IndexController();
if( count( $_POST ) ) {
    $controller->postAction();
}
$template = new TemplateController( __DIR__.'/templete/');
$template->display('head.tpl', array(
    '*|TITLE|*' => 'Test Converter',
    '*|DESCRIPTION|*' => 'WEb aplication for temperature convertetion',
), true);
$template->display('content.tpl', $controller->display(), true);
$template->display('footer.tpl', null, true);

?>