<?php

require_once __DIR__.'/../../../vendor/autoload.php';
use DStaroselskiy\Php\Test\TemperatureConverter\Objects\Temperature;
use DStaroselskiy\Php\Test\TemperatureConverter\Objects\TemperatureConverter;
/**
 * Description of TemperatureTest
 *
 * @author dstaroselskiy
 */
class TemperatureTest extends PHPUnit_Framework_TestCase {

    public function setUp(){ }
    public function tearDown(){ }
    
    public function testKelvinData(){
        $temperature = new Temperature();
        
        $this->AssertTrue( 
            ( $temperature->setValue( 10.5 ) )
            instanceof Temperature
        );
        $this->AssertTrue( 
            ( $temperature->setType( 'kelvin' ) )
            instanceof Temperature
        );
        $this->AssertEquals('kelvin',  $temperature->getType() );   
        
        
        $this->AssertEquals(10.5,  $temperature->getValue() );
        $this->AssertEquals(10.5,  $temperature->getValueInKelvin() );
        $this->AssertEquals(10.5-TemperatureConverter::CELSIUS,  $temperature->getValueInCelsius() );
        $this->AssertTrue( 
            ( $temperature->setValue( 20.5 ) )
            instanceof Temperature
        );
        $this->AssertEquals(20.5,  $temperature->getValue() );
        $this->AssertEquals(20.5,  $temperature->getValueInKelvin() );
        $this->AssertEquals(20.5-TemperatureConverter::CELSIUS,  $temperature->getValueInCelsius() );
        $this->AssertTrue( 
            ( $temperature->setValue( 0 ) )
            instanceof Temperature
        );
        $this->AssertEquals(0,  $temperature->getValue() );
        $this->AssertEquals(0,  $temperature->getValueInKelvin() );
        $this->AssertEquals(0-TemperatureConverter::CELSIUS,  $temperature->getValueInCelsius() );
        $this->AssertTrue( 
            ( $temperature->setValue( 100 ) )
            instanceof Temperature
        );
        $this->AssertEquals(100,  $temperature->getValue() );
        $this->AssertEquals(100,  $temperature->getValueInKelvin() );
        $this->AssertEquals(100-TemperatureConverter::CELSIUS,  $temperature->getValueInCelsius() );
    }
    public function test_KeLVin_Data(){
        $temperature = new Temperature();
        
        $this->AssertTrue( 
            ( $temperature->setValue( 10.5 ) )
            instanceof Temperature
        );
        $this->AssertTrue( 
            ( $temperature->setType( 'KeLVin' ) )
            instanceof Temperature
        );
        $this->AssertNotEquals('KeLVin',  $temperature->getType() );   
        $this->AssertEquals('kelvin',  $temperature->getType() );   
        
        
        $this->AssertEquals(10.5,  $temperature->getValue() );
        $this->AssertEquals(10.5,  $temperature->getValueInKelvin() );
        $this->AssertEquals(10.5-TemperatureConverter::CELSIUS,  $temperature->getValueInCelsius() );
        $this->AssertTrue( 
            ( $temperature->setValue( 20.5 ) )
            instanceof Temperature
        );
        $this->AssertEquals(20.5,  $temperature->getValue() );
        $this->AssertEquals(20.5,  $temperature->getValueInKelvin() );
        $this->AssertEquals(20.5-TemperatureConverter::CELSIUS,  $temperature->getValueInCelsius() );
        $this->AssertTrue( 
            ( $temperature->setValue( 0 ) )
            instanceof Temperature
        );
        $this->AssertEquals(0,  $temperature->getValue() );
        $this->AssertEquals(0,  $temperature->getValueInKelvin() );
        $this->AssertEquals(0-TemperatureConverter::CELSIUS,  $temperature->getValueInCelsius() );
        $this->AssertTrue( 
            ( $temperature->setValue( 100 ) )
            instanceof Temperature
        );
        $this->AssertEquals(100,  $temperature->getValue() );
        $this->AssertEquals(100,  $temperature->getValueInKelvin() );
        $this->AssertEquals(100-TemperatureConverter::CELSIUS,  $temperature->getValueInCelsius() );
    }
    
    public function testCelsiusData(){
        $temperature = new Temperature();
        
        $this->AssertTrue( 
            ( $temperature->setValue( 10.5 ) )
            instanceof Temperature
        );
        $this->AssertTrue( 
            ( $temperature->setType( 'celsius' ) )
            instanceof Temperature
        );
        $this->AssertEquals('celsius',  $temperature->getType() );   
        $this->AssertEquals(10.5,  $temperature->getValue() );
        $this->AssertEquals(10.5+TemperatureConverter::CELSIUS,  $temperature->getValueInKelvin() );
        $this->AssertEquals(10.5, $temperature->getValueInCelsius() );
        $this->AssertTrue( 
            ( $temperature->setValue( 20.5 ) )
            instanceof Temperature
        );
        $this->AssertEquals(20.5,  $temperature->getValue() );
        $this->AssertEquals(20.5+TemperatureConverter::CELSIUS,  $temperature->getValueInKelvin() );
        $this->AssertEquals(20.5,  $temperature->getValueInCelsius() );
        $this->AssertTrue( 
            ( $temperature->setValue( 0 ) )
            instanceof Temperature
        );
        $this->AssertEquals(0,  $temperature->getValue() );
        $this->AssertEquals(0+TemperatureConverter::CELSIUS,  $temperature->getValueInKelvin() );
        $this->AssertEquals(0,  $temperature->getValueInCelsius() );
        $this->AssertTrue( 
            ( $temperature->setValue( 100 ) )
            instanceof Temperature
        );
        $this->AssertEquals(100,  $temperature->getValue() );
        $this->AssertEquals(100+TemperatureConverter::CELSIUS,  $temperature->getValueInKelvin() );
        $this->AssertEquals(100,  $temperature->getValueInCelsius() );
    }
   
    public function test_CeLSius_Data(){
        $temperature = new Temperature();
        
        $this->AssertTrue( 
            ( $temperature->setValue( 10.5 ) )
            instanceof Temperature
        );
        $this->AssertTrue( 
            ( $temperature->setType( 'CeLSius' ) )
            instanceof Temperature
        );
        $this->AssertNotEquals('CeLSius',  $temperature->getType() );   
        $this->AssertEquals('celsius',  $temperature->getType() );   
        $this->AssertEquals(10.5,  $temperature->getValue() );
        $this->AssertEquals(10.5+TemperatureConverter::CELSIUS,  $temperature->getValueInKelvin() );
        $this->AssertEquals(10.5, $temperature->getValueInCelsius() );
        $this->AssertTrue( 
            ( $temperature->setValue( 20.5 ) )
            instanceof Temperature
        );
        $this->AssertEquals(20.5,  $temperature->getValue() );
        $this->AssertEquals(20.5+TemperatureConverter::CELSIUS,  $temperature->getValueInKelvin() );
        $this->AssertEquals(20.5,  $temperature->getValueInCelsius() );
        $this->AssertTrue( 
            ( $temperature->setValue( 0 ) )
            instanceof Temperature
        );
        $this->AssertEquals(0,  $temperature->getValue() );
        $this->AssertEquals(0+TemperatureConverter::CELSIUS,  $temperature->getValueInKelvin() );
        $this->AssertEquals(0,  $temperature->getValueInCelsius() );
        $this->AssertTrue( 
            ( $temperature->setValue( 100 ) )
            instanceof Temperature
        );
        $this->AssertEquals(100,  $temperature->getValue() );
        $this->AssertEquals(100+TemperatureConverter::CELSIUS,  $temperature->getValueInKelvin() );
        $this->AssertEquals(100,  $temperature->getValueInCelsius() );
    }
   
}
