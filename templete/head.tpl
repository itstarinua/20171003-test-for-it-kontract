<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>*|TITLE|*</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link  rel="stylesheet" href="web/css/bootstrap.min.css" />
        <link  rel="stylesheet" href="web/css/bootstrap-theme.min.css" />
        <link  rel="stylesheet" href="web/css/bootstrap-select.min.css" />
        <script type="text/javascript" src="web/js/jquery-2.2.4.min.js"></script>
        <script type="text/javascript" src="web/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="web/js/bootstrap-select.min.js"></script>
    </head>
    