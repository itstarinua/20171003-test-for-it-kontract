<?php
/***************************************************
Version: 0.2
Date: 2016-12-04
****************************************************/

namespace DStaroselskiy\Php\Test\TemperatureConverter\Components;

Class TemplateController
    {
        protected $main_dir = '';
        /** function writes to the variable the path of template folder where to look for the templates for output
         * 
         * @param  string $dir - path to the module where to search for templates for output
         * @return $this
         */
        protected function setMainDir( $dir )
        {
            $this->main_dir = $dir;
            return $this;
        }
        /** function returns the path of template folder where to search for templates for output
         * 
         * @return string
         */
        protected function getMainDir()
        {
            return $this->main_dir;
        }   
        /** load and return content from template file
         * 
         * @return string
         */
        protected function loadTemplate( $file_name ) {            
            $filelink = $this->getMainDir().'/'.$file_name;
            $content = "File for template name = \'".$file_name."\' is apsend";
            
            if( $filelink )
            {
                $content = \file_get_contents( $filelink );
            }
            return $content;
        }
        /** load and return content from template file
         * 
         * @return string
         */
        public function getTemplate( $file_name )
        {
            return $this->loadTemplate( $file_name );
        }
        /** function of preparation of a template to a lead out Replace all inscriptions of the form '* | PARAMETER | *', on the transferred values
         * 
         * @param string $template_name - name of template file
         * @param array $args - array by transferred values
         *                          key - inscriptions of value, 
         *                          data - value
         * @param boolean $echo - flag that indicates the output of the processed template on the screen
         * 
         * @return string
         */
        public function display( $template_name, $args = null, $echo = true)
        {   
            $template = $this->getTemplate( $template_name );
            if( \is_array($args) && ( \count( $args ) > 0 ) )
            {
                $array_parameters = $array_dates = array();
                foreach($args as $key => $date )
                {
                        $array_parameters[] = $key;
                        $array_dates[] = $date;							
                }							
                $template = \str_replace( $array_parameters, $array_dates, $template);
                unset( $array_parameters );
                unset( $array_dates );
            }                            
            if( $echo ) echo $template;
            return $template;	
        }
        /** object constructor
         * 
         * @param string $main_dir - path to tempale folder
         */
        public function __construct( $main_dir ) {
            $this->setMainDir(  $main_dir );
        }
    }

