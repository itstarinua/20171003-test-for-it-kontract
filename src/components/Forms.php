<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace DStaroselskiy\Php\Test\TemperatureConverter\Components;

/**
 * Description of Forms
 *
 * @author dstaroselskiy
 */
class Forms {
    /** save object for get/set form data
     *
     * @var object 
     */
    protected $obj;
    public function __construct( &$obj ) {
        $this->obj = &$obj;
        $this->initAvailebleField();
        $this->initAvailebleFooterButton();
    }
    /** form submit status - flag for check is form submited
     *
     * @var bool
     */
    protected $issubmitted = false;
    /** return flag on form submit status 
     * 
     * @return bool
     */
    public function isSubmit(){
        return $this->issubmitted;
    }
    /** return flag on form submit errors 
     * 
     * @return bool
     */
    public function isSubmitErrors(){
        return $this->isSubmit() && ( count( $this->errors ) != 0 ) ? true : false;
    }
    /** array for save form errors
     *
     * @var array 
     */
    protected $errors = array();
    /** add error information to the error's array
     * 
     * @param string $field
     * @param string $messange
     * @return $this
     */
    protected function addError( $field, $messange){
        $this->errors[ $field ][] = $messange;
        return $this;
    }
    /** function get error messange for field 
     * 
     * @param string $field
     * @return string
     */
    protected function getErrorMessange( $field ){
        $return = '';
        if( $this->isSubmit() && isset( $this->errors[ $field ] ) ){            
            foreach( $this->errors[ $field ] as $messange )
            {
                $return .= '<p class="field-error alert alert-danger">'.$messange.'</p>'.PHP_EOL;
            }             
        }
        return $return;
    }
    /** filter field request resalts
     * 
     * @param array $field_name
     * @param string $value
     * @return mix
     */
    public function getCheckField($field_name, $value){
        $field_options = $this->getAvailableField( $field_name );

        if( isset( $field_options["required"]) 
            && ( $field_options["required"] == true )
            && ( mb_strlen($value) == 0 )
        ) {
            $this->addError($field_name, 'Field "'.$field_options["name"].'" can\'t be empty.');
        }
        else{
            switch( strtolower( $field_options['type'] ) ) :
                case 'number': return $this->checkFieldFieldNumber($field_options, $value);
                case 'select': return $this->checkFieldFieldSelect($field_options, $value);
            endswitch;
        }
        return '';
    }
    /** check value from field number type
     * 
     * @param array $field_options
     * @param string $value
     * @return int|float
     */
    protected function checkFieldFieldNumber($field_options, $value){
        $value = str_replace(' ', '', $value);
        $value = str_replace(',', '.', $value);
        return strpos($value, '.') === false ? intval($value) : floatval($value);
    }
    /** check value from field select type
     * 
     * @param array $field_options
     * @param string $value
     * @return mix
     */
    protected function checkFieldFieldSelect($field_options, $value){
        if( isset($field_options['list']) && count($field_options['list']) && in_array( $value, $field_options['list']) ) {
            return $value;
        }
        $this->addError($field_options["name"], 'Field "'.$field_options["name"].'" have not correct data.');
        return '';
    }
    /** get data from post array
     * 
     * @param string $field_name
     * @return string|false
     */    
    protected function getPostField( $field_name ){
        return isset($_POST[ $field_name ]) ? $_POST[ $field_name ] : false;
    }
    /** get all vars for request and set to the object
     * 
     * @return $this
     */
    public function Request(){         
        if( $this->detectSubmitButton() ){
            foreach($this->getAvailableFields() as $field_name => $field_options) {
                $method = 'set'.ucfirst($field_name);
                if( method_exists( $this->obj, $method) )
                {
                    $this->obj->$method( 
                        $this->getCheckField( $field_name, $this->getPostField( $field_name ) )
                    );
                }
                else
                {
                    $this->addError($field_name, 'Field "'.$field_options["name"].'" can\'t be add to the object.');
                }
            }
        }
        return $this;
    }
    /** get value of object for field by name
     * 
     * @param string $field_name
     * @return mix
     */
    protected function getObjectValue( $field_name ){
        $method = 'get'.ucfirst($field_name);
        if( method_exists( $this->obj, $method) )
        {
            return $this->obj->$method();
        }
        $this->addError($field_name, 'Field "'.$field_name.'" can\'t get data for field.');
        return false;
    }
    /** array for save form buttons
     *
     * @var array 
     */
    protected $buttons = array(
        'footer' => array(            
        ),
    );
    /** get template of button
     * 
     * @return string
     */
    public function getTemplateButtonFooter(){
        $buttons = '';
        foreach($this->buttons['footer'] as $field => $field_data)
        {
            $buttons .= $this->getTemplateField($field, $field_data);
        }
        return <<<STR
                <p>
                    {$buttons}
                </p>        
STR;
    }
    /** save name of button wich submited form
     *
     * @var string 
     */
    protected $submit_by_button = '';
    /** function return key of submited button or test send key with submited buttons key
     * 
     * @param string $key
     * @return bool string
     */
    public function submitedButton( $key = false ){
        if( $key ){
            return ( $this->submit_by_button == $key );
        }
        return $this->submit_by_button;
    }
    /** function detect witch button sabmited form
     * 
     * @return boolean
     */
    protected function detectSubmitButton(){
        if( count( $this->buttons ) ) {
            foreach ( $this->buttons as $level )
            {
                if( count( $level ) ){
                    foreach ( $level as $field_name => $field_options )
                    {
                        if( ($field_options['type'] == 'submit') 
                            && isset( $_POST[ $field_name] ) 
                            && ( $_POST[ $field_name] == $field_options['name'] ) )
                        {
                            $this->submit_by_button = $field_name;
                            $this->issubmitted = true;
                            return true;
                        }
                    }                    
                }
            }
        }
        return false;
    }
    /** get template of form field by type number
     * 
     * @param string $field
     * @param array $field_data
     * @return string
     */
    public function getTemplateFieldNumber( $field, $field_data ){
        $placeholder = isset($field_data['placeholder']) && !empty($field_data['placeholder'])? $field_data['placeholder'] : $field_data['name'];
        $required = isset($field_data['required']) && $field_data['required'] ? 'required' : '';
        $required_lable = isset($field_data['required']) && $field_data['required'] ? ' *' : '';
        return <<<STR
        <div class="input-group">
            <span class="input-group-addon">{$field_data['name']}{$required_lable}</span>
            <input type="text" class="form-control number field-{$field}" name="{$field}" value="{$field_data['value']}" placeholder="{$placeholder}" />     
        </div>
        {$this->getErrorMessange($field)}    
STR;
    }
    /** get template of form field by type button
     * 
     * @param string $field
     * @param array $field_data
     * @return string
     */
    public function getTemplateFieldButton( $field, $field_data ){
        return <<<STR
            <input class="btn btn-default" name="{$field}" type="{$field_data['type']}" value="{$field_data['name']}">
STR;
    }
    /** get template of form field by type select
     * 
     * @param string $field
     * @param array $field_data
     * @return string
     */
    public function getTemplateFieldSelect( $field, $field_data ){
        $required = $field_data['required'] ? 'required' : '';
        $required_lable = $field_data['required'] ? ' *' : '';
        $options = '';        
        if( empty($field_data['value']) )
        {
            $options = <<<STR
            <option value="" selected> --- </option>
STR;
        }
        foreach ( $field_data['list'] as $value => $field_option ) {
                $selected = '';
                if( !is_string( $value ) ) {
                    $value = $field_option;
                }
                if( $field_data['value'] == $value ){
                    $selected = 'selected';
                }
                $options .= <<<STR
            <option value="{$value}" {$selected}>{$field_option}</option>
STR;
        }        
        return <<<STR
        <div class="input-group">
            <span class="input-group-addon">{$field_data['name']}{$required_lable}</span>
            <select class="selectpicker" name="{$field}">
                {$options}
             </select>
        </div>
        {$this->getErrorMessange($field)}       
STR;
    }
     /** get template of form field
     * 
     * @param string $field
     * @param array $field_data
     * @return string
     */
    public function getTemplateField( $field, $field_data )
    {
        switch( strtolower( $field_data['type'] ) ){
            case 'number': return $this->getTemplateFieldNumber($field, $field_data);
            case 'select': return $this->getTemplateFieldSelect($field, $field_data);
            case 'reset': 
            case 'button': 
            case 'submit': return $this->getTemplateFieldButton($field, $field_data);
        }
        return '';
    }     
    /** array for save list of forms fields
     *
     * @var array 
     */
    protected $available_fields = array();
    /** add new field to the list of forms field
     * 
     * @param string $name
     * @param array $options
     * @return $this
     */
    protected function addAvailebleField( $name, $options ){
        $this->available_fields[$name] = $options;
        return $this;
    }
    /** add new button to the list of forms field
     * 
     * @param string $name
     * @param array $options
     * @return $this
     */
    protected function addAvailebleFooterButton( $name, $options ){
        $this->buttons['footer'][$name] = $options;
        return $this;
    }
    /** function for add all forms field to the list of forms field
     * @return $this
     */
    public function initAvailebleField(){
        return $this;
    }
    /** function for add all forms buttons to the list of forms buttons
     * @return $this
     */
    public function initAvailebleFooterButton(){
    }
    /** get all fields of form
     * 
     * @return array
     */
    public function getAvailableFields(){
        return $this->available_fields;
    }
    /** get field options by field name
     * 
     * @param string $key
     * @return array
     */
    public function getAvailableField( $key ){
        return isset($this->available_fields[ $key ]) ? $this->available_fields[ $key ] : false;
    }  
    /** get template of form
     * 
     * @return string
     */
    public function getForm(){
        $content = '';
        foreach ( $this->getAvailableFields() as $field => $available_field) { 
            $available_field['value'] = $this->getObjectValue( $field );
            $content .= '<div class="">';
            $content .= $this->getTemplateField($field, $available_field);                     
            $content .= '</div>';
        }
        return <<<STR
            <form action="" method="POST" class="">
                {$this->getErrorMessange('main')}
                {$this->filterContent( $content )}
                <div class="text-center">
                    {$this->getTemplateButtonFooter()}
                </div>
            </form>
STR;
    }
    
    public function filterContent( $content ){
        return $content;
    }
}
