<?php

namespace DStaroselskiy\Php\Test\TemperatureConverter\Objects;
use DStaroselskiy\Php\Test\TemperatureConverter\Objects\TemperatureConverter;
/**
 * Description of Temperature 
 *
 * @author dstaroselskiy
 */
class Temperature extends TemperatureConverter{
    /** value of temperature
     *
     * @var float
     */
    protected $value = 0;
    /** type of temperature
     *
     * @var string 
     */
    protected $type = 'none';
    /** set type of temperature to the object
     * 
     * @param string $type
     * @return $this
     */
    public function setType( $type ){
        $this->type = strtolower( $type );
        return $this;
    }
    /** set value of temperature to the object
     * 
     * @param float|int $value
     * @return $this
     */
    public function setValue( $value ){
        $this->value = $value;
        return $this;
    }
    /** get the type of temperature by object
     * 
     * @return string
     */
    public function getType(){
        return $this->type;
    }
    /** get value of temperature by object
     * 
     * @return string
     */
    public function getValue(){
        return $this->value;
    }
    /** convertated and return value of temperature in type, witch we need
     * 
     * @param string $in_type_name - the type of return value by temperature
     * @return flout | boolean - return value or false if can be convertid
     */
    protected function getValueIn( $in_type_name ) {
        $fierst_part_of_name = strtolower( $this->getType() );
        $second_part_of_name = strtolower( $in_type_name );
        if( $fierst_part_of_name === $second_part_of_name ) {
            return $this->getValue();
        }
        $method_name = $fierst_part_of_name.'To'.ucfirst( $second_part_of_name );
        if( method_exists($this, $method_name) ) {
            return $this->$method_name( $this->getValue() );
        }
        return false;
    }
    /** get value of temperature in kalvin type
     * 
     * @return flout | boolean - return value or false if can be convertid
     */
    public function getValueInKelvin(){
        return $this->getValueIn( 'kelvin' );
    }
    /** get value of temperature in celsius type
     * 
     * @return flout | boolean - return value or false if can be convertid
     */
    public function getValueInCelsius(){
        return $this->getValueIn( 'celsius' );
    }
}
