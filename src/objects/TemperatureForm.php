<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace DStaroselskiy\Php\Test\TemperatureConverter\Objects;
use DStaroselskiy\Php\Test\TemperatureConverter\Components\Forms;
/**
 * Description of TemperatureForm
 *
 * @author dstaroselskiy
 */
class TemperatureForm extends Forms {
    /** add all button to the form 
     * 
     * @return this
     */
    public function initAvailebleFooterButton(){
        $this->addAvailebleFooterButton( "CelsiusToKelvin" , [
                "type" => "submit",
                "name" => "Show in Kelvin",
            ]
        );
        $this->addAvailebleFooterButton( "KelvinToCelsius" , [
                "type" => "submit",
                "name" => "Show in Celsius",
            ]
        );
        return parent::initAvailebleFooterButton();
    }
    /** add all field to the form 
     * 
     * @return this
     */    
    public function initAvailebleField(){
        $this->addAvailebleField( "value" , [
                "type" => "number",
                "name" => "Temperature",
                'required' => true
            ]                
        )->addAvailebleField( "type" , [
                "type" => "select",
                "name" => "Type",
                "list" => [
                    "Celsius",
                    "Kelvin"
                ],
                'required' => true
            ]
        );
        return parent::initAvailebleField();
    }
   
}
