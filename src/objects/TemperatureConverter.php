<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace DStaroselskiy\Php\Test\TemperatureConverter\Objects;

/**
 * Description of TemperatureConverter
 *
 * @author dstaroselskiy
 */
abstract class TemperatureConverter {
    /** 
     * @var flout - the konstant for convertation temperature
     */
    const KELVIN = 0;    
    /** 
     * @var flout - the konstant for convertation temperature, save the celsius count by kelvin
     */
    const CELSIUS = -273.15; //by kelvin
    /** convertated the themperature form celsius to kelvin
     * 
     * @param int|float $celsius - temperature in celsius
     * @return float - temperature in kilvin
     */
    public function celsiusToKelvin( $celsius ){
        return $celsius + self::CELSIUS;        
    }
    /** convertated the themperature form kelvin to celsius
     * 
     * @param int|float $kelvin - temperature in kilvin 
     * @return float - temperature in celsius
     */
    public function kelvinToCelsius( $kelvin ){
        return $kelvin - self::CELSIUS;        
    }    
}
