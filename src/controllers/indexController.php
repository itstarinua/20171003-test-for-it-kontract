<?php

namespace DStaroselskiy\Php\Test\TemperatureConverter\Controllers;
use DStaroselskiy\Php\Test\TemperatureConverter\Objects\TemperatureForm;
use DStaroselskiy\Php\Test\TemperatureConverter\Objects\Temperature;
use DStaroselskiy\Php\Test\TemperatureConverter\Objects\TemperatureConverter;
/**
 * Description of indexController
 *
 * @author dstaroselskiy
 */
class IndexController {
    protected $name ='index';
    protected $obj = null;
    protected $form = null;
    public function __construct() {
        $this->obj = new Temperature();
        $this->form = new TemperatureForm( $this->obj );
    }
    /** function will be start, if &_POST array have data
     * 
     */
    public function postAction(){
        if( $this->form->Request()->isSubmit() ) {
            
        }
    }
    /** prepear data for show in templare
     * 
     * @return array
     */
    public function display(){
        $results = '';
        if( !$this->form->isSubmit() ){
            $results = '';
        }elseif( $this->form->isSubmitErrors() ){
           $results = "Convertetion error";   
        }else{
            if( $this->form->submitedButton("CelsiusToKelvin") ) {
                $results =  $this->obj->getValueInKelvin().' by Kelvin';
            }elseif( $this->form->submitedButton("KelvinToCelsius") ) {
                $results =  $this->obj->getValueInCelsius().' by Celsius';
            }
        }
        return array(
            '*|FORM_TITLE|*' => 'Temperature converter',
            '*|FORM|*' => $this->form->getForm(),
            '*|RESULTS|*' => !empty($results) ? 'Return value: '.$results : '',
        );
    }
}
